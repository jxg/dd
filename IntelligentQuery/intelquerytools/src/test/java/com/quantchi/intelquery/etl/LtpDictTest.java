package com.quantchi.intelquery.etl;

import com.quantchi.intelquery.config.ConfigManager;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.SQLException;
import java.util.Date;
import org.junit.Ignore;
import org.junit.Test;

/**
 * @author hetong.
 */
public class LtpDictTest {
  @Ignore
  @Test
  public void ltpEntityTest() {
    String prefix = ConfigManager.getInstance().getQPConfigValue("tdb.uri_prefix");
    String tdb = ConfigManager.getInstance().getQPConfigValue("tdb.folder");
    String model = ConfigManager.getInstance().getQPConfigValue("tdb.model");
    LtpEntity dict = new LtpEntity(tdb, model, prefix);
    System.out.println(dict.genLtpDict());
  }

  @Ignore
  @Test
  public void ltpStrValTest() throws Exception {
    LtpName dict = new LtpName.Builder()
        .jdbcUrl("jdbc:hive2://192.168.1.21:10000/ctdata")
        .username("hive")
        .password("hive")
        .dbType("hive")
        .nameField("dim_customer.customer_name")
        .createTimeField("confirm_date")
        .dateFormat("yyyyMMdd")
        .startTime(new Date(1483228800))
        .build();
    BufferedWriter writer = new BufferedWriter(new FileWriter("/tmp/names"));
    writer.write(dict.genLtpDict());
    writer.close();
  }

}
