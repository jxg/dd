package com.quantchi.intelquery.etl;

import com.quantchi.intelquery.config.ConfigManager;
import com.quantchi.intelquery.utils.TdbConnection;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;
import org.apache.jena.query.Dataset;
import org.apache.jena.tdb.TDBFactory;
import org.junit.Ignore;
import org.junit.Test;

public class EtlTest {

  public static void main(String[] args) throws Exception {
    Properties qpConfig = ConfigManager.getInstance().getQPConfig();
    String ontModelFilePath = Etl.class.getClassLoader().getResource("kg.owl").getFile();
    TdbConnection tdbConnection = new TdbConnection(qpConfig.getProperty("tdb.folder"), qpConfig.getProperty("tdb.model"));
    Dataset ds = TDBFactory.createDataset(qpConfig.getProperty("tdb.folder"));
    TdbUtils.setModel(ds, qpConfig.getProperty("tdb.model"), ontModelFilePath, true);
    tdbConnection.execSelect("SELECT * {?s ?p ?o}", true);
    tdbConnection.execSelect("SELECT (count(*) AS ?count) {?s ?p ?o} LIMIT 100", true);
  }

  @Ignore
  @Test
  public void etlTest() throws SQLException, IOException {
    new Etl("kg.owl");
  }
}
