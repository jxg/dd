package com.liangzhi.autoCode;

import static com.liangzhi.autoCode.AutoCodeUtil.captureName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreateCodeService {
  @Autowired
  private AutoCodeMapper mapper;

  @Autowired
  private AutoCodeMapperKylin service;
  
  @Autowired
  private AutoCodeMapperHive hiveService;
  
  public void start(AutoCode code) {
    // 所需要参数
    Map<String, Object> map = getJavaObject(code);
    // java类名(首字母大写)
    String className = captureName(AutoCodeUtil.getBeanName(code.getTableName()));
    // ***************生成实体类**********************//
    // 模版路径+名称
    StringBuffer entityPathAndName = AutoCodeUtil.getPathByCom(code.getBasePackage());
    entityPathAndName.append("autoCode/ftl/entity.ftl");
    // 输出路径+名称
    StringBuffer entityOutFile =
        AutoCodeUtil.getPathByCom(code.getBasePackage() + "." + code.getEntity());
    AutoCodeUtil.getPath(entityOutFile.toString());// 先看是否存在路径没有先创建
    entityOutFile.append(className + ".java");
    AutoCodeUtil.getNewFile(entityPathAndName.toString(), map, entityOutFile.toString());
    System.out.println(
        "-------------------------------------" + AutoCodeUtil.getBeanName(code.getTableName())
            + "实体类生成完毕------------------------------------");
    // ***************生成dao类**********************//
    // 模版路径+名称
    StringBuffer daoPathAndName = AutoCodeUtil.getPathByCom(code.getBasePackage());
    daoPathAndName.append("autoCode/ftl/dao.ftl");
    // 输出路径+名称
    StringBuffer daoOutFile =
        AutoCodeUtil.getPathByCom(code.getBasePackage() + "." + code.getDao());
    AutoCodeUtil.getPath(daoOutFile.toString());// 先看是否存在路径没有先创建
    daoOutFile.append(className + "Dao.java");
    AutoCodeUtil.getNewFile(daoPathAndName.toString(), map, daoOutFile.toString());
    System.out.println(
        "-------------------------------------" + AutoCodeUtil.getBeanName(code.getTableName())
            + "Dao类生成完毕------------------------------------");
    // ***************生成service接口类**********************//
    // 模版路径+名称
    StringBuffer servicePathAndName = AutoCodeUtil.getPathByCom(code.getBasePackage());
    servicePathAndName.append("autoCode/ftl/service.ftl");
    // 输出路径+名称
    StringBuffer serviceOutFile =
        AutoCodeUtil.getPathByCom(code.getBasePackage() + "." + code.getService());
    AutoCodeUtil.getPath(serviceOutFile.toString());// 先看是否存在路径没有先创建
    serviceOutFile.append(className + "Service.java");
    AutoCodeUtil.getNewFile(servicePathAndName.toString(), map, serviceOutFile.toString());
    System.out.println(
        "-------------------------------------" + AutoCodeUtil.getBeanName(code.getTableName())
            + "Service接口类生成完毕-----------------------------");
    // ***************生成serviceImpl实现类**********************//
    // 模版路径+名称
    StringBuffer serviceImplPathAndName = AutoCodeUtil.getPathByCom(code.getBasePackage());
    serviceImplPathAndName.append("autoCode/ftl/serviceImpl.ftl");
    // 输出路径+名称
    StringBuffer serviceImplOutFile =
        AutoCodeUtil.getPathByCom(code.getBasePackage() + "." + code.getServiceImpl());
    AutoCodeUtil.getPath(serviceImplOutFile.toString());// 先看是否存在路径没有先创建
    serviceImplOutFile.append(className + "ServiceImpl.java");
    AutoCodeUtil.getNewFile(serviceImplPathAndName.toString(), map, serviceImplOutFile.toString());
    System.out.println(
        "-------------------------------------" + AutoCodeUtil.getBeanName(code.getTableName())
            + "ServiceImpl实现类生成完毕--------------------------");
    // ***************生成controller实现类**********************//
    // 模版路径+名称
    StringBuffer controllerPathAndName = AutoCodeUtil.getPathByCom(code.getBasePackage());
    controllerPathAndName.append("autoCode/ftl/controller.ftl");
    // 输出路径+名称
    StringBuffer controllerOutFile =
        AutoCodeUtil.getPathByCom(code.getBasePackage() + "." + code.getController());
    AutoCodeUtil.getPath(controllerOutFile.toString());// 先看是否存在路径没有先创建
    controllerOutFile.append(className + "Controller.java");
    AutoCodeUtil.getNewFile(controllerPathAndName.toString(), map, controllerOutFile.toString());
    System.out.println(
        "-------------------------------------" + AutoCodeUtil.getBeanName(code.getTableName())
            + "Controller实现类生成完毕----------------------------");
    // ***************生成mapper类**********************//
    // 模版路径+名称
    StringBuffer mapperPathAndName = AutoCodeUtil.getPathByCom(code.getBasePackage());
    mapperPathAndName.append("autoCode/ftl/mapper.ftl");
    // 输出路径+名称
    StringBuffer mapperOutFile =
        AutoCodeUtil.getPathByCom(code.getBasePackage() + "." + code.getMapper());
    AutoCodeUtil.getPath(mapperOutFile.toString());// 先看是否存在路径没有先创建
    mapperOutFile.append(className + "Mapper.xml");
    AutoCodeUtil.getNewFile(mapperPathAndName.toString(), map, mapperOutFile.toString());
    System.out.println(
        "-------------------------------------" + AutoCodeUtil.getBeanName(code.getTableName())
            + "mapper文件生成完毕------------------------------------");
  }

  private Map<String, Object> getJavaObject(AutoCode code) {
    List<Map<String, Object>> list = new ArrayList<>();
    if("mysql".equals(code.getModuleName())){
      list = mapper.selectTable(code.getTableSchema(), code.getTableName());
    }else if("kylin".equals(code.getModuleName())){
      list = service.selectKylinRelevance();
    }else if("hive".equals(code.getModuleName())){
      list = hiveService.selectHive();
    }else{
      list = service.selectKylin(code.getTableName());
    }
    Map<String, Object> map = new HashMap<String, Object>();
    // 原来表名
    map.put("tableName", code.getTableName());
    // 模块名称---解释使用
    map.put("moduleName", code.getModuleName());
    // java类名称(首字母小写)
    map.put("className", AutoCodeUtil.getBeanName(code.getTableName()));
    // 基础包
    map.put("basePackage", code.getBasePackage());
    // 实体类包名
    map.put("entityPackage", code.getBasePackage() + "." + code.getEntity());
    // dao类包名
    map.put("daoPackage", code.getBasePackage() + "." + code.getDao());
    // mapper文件包名
    map.put("mapperPackage", code.getBasePackage() + "." + code.getMapper());
    // service类包名
    map.put("servicePackage", code.getBasePackage() + "." + code.getService());
    // serviceImpl类包名
    map.put("serviceImplPackage", code.getBasePackage() + "." + code.getServiceImpl());
    // controller类包名
    map.put("controllerPackage", code.getBasePackage() + "." + code.getController());
    List<Map<String, Object>> l = new ArrayList<>();
    for (int i = 0; i < list.size(); i++) {
      Map<String, Object> typeMap = new HashMap<>();
      // 原来数据库字段名
      typeMap.put("columnName", list.get(i).get("COLUMN_NAME").toString());
      // javaBean字段是否为空
      if (list.get(i).get("IS_NULLABLE") != null
          && list.get(i).get("IS_NULLABLE").toString().equals("NO")) {
        typeMap.put("isNull", false);
      } else {
        typeMap.put("isNull", true);
      }
      // javaBean字段名
      typeMap.put("beanName", AutoCodeUtil.getBeanName(list.get(i).get("COLUMN_NAME").toString()));
      // 是否为KEY
      if (list.get(i).get("COLUMN_KEY") != null
          && list.get(i).get("COLUMN_KEY").toString().equals("PRI")) {
        typeMap.put("id", true);
      } else {
        typeMap.put("id", false);
      }
      // 是否有外键
      if (list.get(i).get("COLUMN_KEY") != null
          && list.get(i).get("COLUMN_KEY").toString().equals("MUL")) {
        typeMap.put("mul", true);
      } else {
        typeMap.put("mul", false);
      }
      // 是否为自增
      if (list.get(i).get("EXTRA") != null && list.get(i).get("EXTRA").toString().length() > 0) {
        typeMap.put("extpa", true);
      } else {
        typeMap.put("extpa", false);
      }
      // 由数据库类型转JAVA类型
      String str =list.get(i).get("DATA_TYPE").toString();
      if(str.contains("VARCHAR")){
        str = "VARCHAR";
      }
      String s = AutoCodeUtil.ColumnTypeEnum
          .getColumnTypeEnumByDBType(str.toUpperCase());
      int i1 = s.lastIndexOf(".");
      typeMap.put("type", s.substring(i1 + 1));
      // 备注,仅用于实体类,是可无的对象
      if (list.get(i).get("COLUMN_COMMENT") != null) {
        typeMap.put("commentName", list.get(i).get("COLUMN_COMMENT").toString());
      }
      l.add(typeMap);
    }
    map.put("data", l);
    return map;
  }
}
