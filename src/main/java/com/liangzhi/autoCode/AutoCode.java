package com.liangzhi.autoCode;


/**
 * Created by Administrator on 2017/7/6.
 */
public class AutoCode {

	// 数据名
	private String tableSchema = "test";
	// 表名
	private String tableName = "DEMO";
	// 模块名称
	private String moduleName = "模块名称";
	// 基础类包
	private String basePackage = "com.liangzhi";
	// 实体类名
	private String entity = "entity";
	// dao
	private String dao = "dao";
	private String mapper = "mapper";
	private String service = "service";
	private String serviceImpl = "service.Impl";
	private String controller = "controller";
	// 网页路径
	private String pagePath = "src/main/resources/templates/";

	public AutoCode() {
		
	}
	/**
	 * 构造方法
	 * @param tableName 数据表名
	 * @param moduleName 模块名称
	 */
	public AutoCode( String tableName, String moduleName) {
		super();
		this.tableName = tableName;
		this.moduleName = moduleName;
	}
	/**
	 * 构造方法
	 * @param tableSchema 数据库名称
	 * @param tableName 数据表名
	 * @param moduleName 模块名称
	 */
	public AutoCode(String tableSchema, String tableName, String moduleName) {
		super();
		this.tableSchema = tableSchema;
		this.tableName = tableName;
		this.moduleName = moduleName;
	}

	public String getTableSchema() {
		return tableSchema;
	}

	public void setTableSchema(String tableSchema) {
		this.tableSchema = tableSchema;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getBasePackage() {
		return basePackage;
	}

	public void setBasePackage(String basePackage) {
		this.basePackage = basePackage;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public String getDao() {
		return dao;
	}

	public void setDao(String dao) {
		this.dao = dao;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getServiceImpl() {
		return serviceImpl;
	}

	public void setServiceImpl(String serviceImpl) {
		this.serviceImpl = serviceImpl;
	}

	public String getController() {
		return controller;
	}

	public void setController(String controller) {
		this.controller = controller;
	}

	public String getPagePath() {
		return pagePath;
	}

	public void setPagePath(String pagePath) {
		this.pagePath = pagePath;
	}
  public String getMapper() {
    return mapper;
  }
  public void setMapper(String mapper) {
    this.mapper = mapper;
  }

	
}
