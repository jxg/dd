package ${servicePackage};

import ${entityPackage}.${className?cap_first};
import com.jjmf.util.entity.*;

import java.util.*;

/**
* Created by jiang.
* 时间:${.now?string("yyyy-MM-dd HH:mm:ss")}
*/
public interface ${className?cap_first}Service {
  String updateControl(List<${className?cap_first}> list);
}
