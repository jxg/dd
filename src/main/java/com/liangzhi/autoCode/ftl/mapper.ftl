<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="${daoPackage}.${className?cap_first}Dao">
	<resultMap type="${mapperPackage}.${className?cap_first}" id="BaseResultMap">
	<#list data as d>
	<#if d.columnName == 'ID'>
	 <id column="${d.columnName}" property="${d.beanName}" />
	<#else>	
	<result column="${d.columnName}" property="${d.beanName}" />
	</#if>
	 </#list>
	</resultMap>
	
	<select id="selectControl" resultType="java.util.HashMap">
  select report_id,report_desc,parent_name from report where report_id in (select distinct report_id
                from role_report
             where role_id in (${r"${roleId}"}))
    order by report_id
  </select>
  <select id="selectReport" resultType="java.util.HashMap">
   select report_id,report_desc,parent_name from report
  </select>
  
  <insert id="addControl">
  insert into role_report (role_id,report_id) values
		<foreach collection="lists" index="index" item="role"
			separator=",">
			${r"(#{role.roleId},#{role.reportId})"}
		</foreach>
  </insert>
  
  <delete id="deleteControl">
   delete from role_report
		where
		role_id =${r"#{roleId}"}
  </delete>
	
</mapper>