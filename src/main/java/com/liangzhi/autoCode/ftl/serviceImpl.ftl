package ${serviceImplPackage};

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import ${daoPackage}.${className?cap_first}Dao;
import ${entityPackage}.${className?cap_first};
import ${servicePackage}.${className?cap_first}Service;
import com.jjmf.util.entity.*;
import com.jjmf.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


/**
* Created by jiang.
* 时间:${.now?string("yyyy-MM-dd HH:mm:ss")}
*/
@Service
@Transactional
public class ${className?cap_first}ServiceImpl implements ${className?cap_first}Service {

    @Autowired
    private ${className?cap_first}Mapper mapper;

   @Override
  public String updateControl(List<${className?cap_first}> list) {
    
    ${className?cap_first} ${className} = new ${className?cap_first}();
    StringBuilder roleId = new StringBuilder();
    roleId.append("'").append(list.get(0).getRoleId()).append("'");
    ${className}.setRoleId(roleId.toString());
    List<Map<String, String>> selectControl = mapper.selectControl(${className});
    if (selectControl.isEmpty()) {
      try {
        mapper.addControl(list);
        return JsonResult.successJson();
      } catch (Exception e) {
        e.printStackTrace();
        return JsonResult.errorJson("add control error");
      }
    } else {
      try {
        ${className?cap_first} ${className}1 = new ${className?cap_first}();
        ${className}1.setRoleId(list.get(0).getRoleId());
        mapper.deleteControl(${className}1);
        mapper.addControl(list);
        return JsonResult.successJson();
      } catch (Exception e) {
        e.printStackTrace();
        return JsonResult.errorJson("update control error");
      }
    }
  }
}
