package ${entityPackage};


import org.springframework.format.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

<#list data as d>
    <#if d.type=='Timestamp' ||d.type=='Date'>
import java.util.Date;
    </#if>
</#list>
/**
* Created by jiang.
* 时间:${.now?string("yyyy-MM-dd HH:mm:ss")}
*/
@ApiModel("实体类")
public class ${className?cap_first} {
<#list data as d>
    <#if d.type=="Timestamp">@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")</#if> <#if d.type=="date">@DateTimeFormat(pattern = "yyyy-MM-dd")</#if>
    private <#if d.type=='Timestamp'>Date<#else>${d.type}</#if> ${d.beanName};     
</#list>
    /******************
    * 无参构造器
    *********************/
    public ${className?cap_first}() {
    }

    /*****************
    *有参构造器
    *********************/
    public ${className?cap_first}(<#list data as d><#if d_index==0><#if d.type=='Timestamp'>Date<#else>${d.type}</#if> ${d.beanName}<#else>,<#if d.type=='Timestamp'>Date<#else>${d.type}</#if> ${d.beanName}</#if></#list>) {
<#list data as d>
        this.${d.beanName} = ${d.beanName};
</#list>}

    /****************
    *set和get方法
    ****************/
<#list data as d>
    public <#if d.type=='Timestamp'>Date<#else>${d.type}</#if> get${d.beanName?cap_first}() {
        return ${d.beanName};
    }

    public void set${d.beanName?cap_first}(<#if d.type=='Timestamp'>Date<#else>${d.type}</#if> ${d.beanName}) {
        this.${d.beanName} = ${d.beanName};
    }
</#list>

    /*****************
    *toString
    ***********************/
    @Override
    public String toString() {
        return "${className?cap_first}{" +
            <#list data as d>
            "${d.beanName}='" + ${d.beanName} + '\'' +
            </#list>
        '}';
    }
}
