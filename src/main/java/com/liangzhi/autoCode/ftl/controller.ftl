package ${controllerPackage};

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import ${entityPackage}.${className?cap_first};
import ${servicePackage}.${className?cap_first}Service;
import com.jjmf.util.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
* Created by jiang.
* 时间:${.now?string("yyyy-MM-dd HH:mm:ss")}
*/
@Api("${moduleName}控制器")
@RestController
@RequestMapping("${className?cap_first}")
public class ${className?cap_first}Controller {

    @Autowired
    private ${className?cap_first}Service service;

     @ApiOperation(value = "报表权限添加", notes = "报表权限添加", produces = "application/json")
  @ResponseBody
  @RequestMapping(value = "/addControl", method = {RequestMethod.POST})
  public String addControl(@RequestBody Map<String, String> map) {
    String[] splits = map.get("reportIds").split(",");
    List<${className?cap_first}> list = new ArrayList();
    for (String split : splits) {
      ${className?cap_first} ${className} = new ${className?cap_first}();
      int reportId = Integer.parseInt(split);
      ${className}.setRoleId(map.get("roleId"));
      ${className}.setMenuId(reportId);
      list.add(${className});
    }
    String result = service.updateControl(list);
    return result;
  }
}
