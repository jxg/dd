package ${daoPackage};

import ${entityPackage}.${className?cap_first};
import org.apache.ibatis.annotations.*;

import java.util.*;

/**
* Created by jiang.
* 时间:${.now?string("yyyy-MM-dd HH:mm:ss")}
*/
@Mapper
public interface ${className?cap_first}Dao {

    String getControl(String reportId);

    List<Map<String, String>> selectControl(${className?cap_first} ${className});

    void addControl(@Param("lists")List<${className?cap_first}> list);

    void deleteControl(${className?cap_first} ${className});

}
