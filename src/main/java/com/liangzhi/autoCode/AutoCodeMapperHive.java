package com.liangzhi.autoCode;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2017/7/6.
 */
@Service
@PropertySource("classpath:db_config.properties")
public class AutoCodeMapperHive {

  @Value("${hive.url}")
  private String hiveUrl;

  @Value("${hive.name}")
  private String name;

  @Value("${hive.password}")
  private String password;

  @Value("${sql}")
  private String sql;

  @Value("${hive.driverName}")
  private String driverName;

  public List<Map<String, Object>> selectHive() {
    Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
    try {
      Class.forName(driverName);
      conn = DriverManager.getConnection(hiveUrl, name, password);
      pstmt = conn.prepareStatement(sql);
      rs = pstmt.executeQuery();
      ResultSetMetaData md = rs.getMetaData(); // 获得结果集结构信息,元数据
      int columnCount = md.getColumnCount(); // 获得列数
      String[] split;
      for (int i = 1; i <= columnCount; i++) {
        Map<String, Object> rowData = new HashMap<String, Object>();
        if (md.getColumnName(i).contains(".")) {
          split = md.getColumnName(i).split(".");
          rowData.put("COLUMN_NAME", split[1].toString());
        }else{
          rowData.put("COLUMN_NAME", md.getColumnName(i).toString());
        }
        rowData.put("DATA_TYPE", md.getColumnTypeName(i).toString());
        list.add(rowData);
      }
      
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      if (rs != null) { // 关闭记录集
        try {
          rs.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
      if (pstmt != null) { // 关闭声明
        try {
          pstmt.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
      if (conn != null) { // 关闭连接对象
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return list;
  }
}
