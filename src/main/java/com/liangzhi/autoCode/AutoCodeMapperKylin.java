package com.liangzhi.autoCode;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Created by Administrator on 2017/7/6.
 */
@Service
@PropertySource("classpath:db_config.properties")
public class AutoCodeMapperKylin {
  private  String encoding;
 
  @Value("${kylin.url}")
  private  String kylinUrl;
  
  @Value("${kylin.project.name}")
  private  String kylinProjectName;
  
  @Value("${kylin.name}")
  private  String name;
  
  @Value("${kylin.password}")
  private  String password;
  
  @Value("${sql}")
  private  String sql;
    
  public List<Map<String, Object>> selectKylin(String table) {
    Map<String, List<Map<String, Object>>> kylinResult = getKylinResult();
   return  kylinResult.get(table);
  } 
  private  String login() {
    String method = "POST";
    String para = "/user/authentication";
    byte[] key = (name + ":" + password).getBytes();
    encoding = Base64.encodeBase64String(key);
    return excute(para, method, null);
  }

  private  String excute(String para, String method, String body) {

    StringBuilder out = new StringBuilder();
    try {
      URL url = new URL(kylinUrl + para);
      HttpURLConnection connection = (HttpURLConnection) url.openConnection();
      connection.setRequestMethod(method);
      connection.setDoOutput(true);
      connection.setRequestProperty("Authorization", "Basic " + encoding);
      connection.setRequestProperty("Content-Type", "application/json");
      if (body != null) {
        byte[] outputInBytes = body.getBytes("UTF-8");
        OutputStream os = connection.getOutputStream();
        os.write(outputInBytes);
        os.close();
      }
      InputStream content = (InputStream) connection.getInputStream();
      BufferedReader in = new BufferedReader(new InputStreamReader(content));
      String line;
      while ((line = in.readLine()) != null) {
        out.append(line);
      }
      in.close();
      connection.disconnect();

    } catch (Exception e) {
      e.printStackTrace();
    }
    System.out.println(out.toString());
    return out.toString();
  }
  
  private Map<String,List<Map<String, Object>>> getKylinResult() {
    login();
    String method = "GET";
    String para = "/tables_and_columns?project="+kylinProjectName;
    String result =  excute(para, method, null);
    JSONArray json = JSONArray.fromObject(result);
    Map<String,List<Map<String, Object>>> map1 = new HashMap<>(); 
    for(Object obj:json){
      JSONArray columns = (JSONArray) ((JSONObject)obj).get("columns");
      String tableName = ((JSONObject)obj).get("table_NAME").toString();
      List<Map<String, Object>> list1 = new ArrayList<>();
      for(Object obj1:columns){
        Map<String, Object> map = new HashMap();
          map.put("COLUMN_NAME", ((JSONObject)obj1).get("column_NAME").toString());
          map.put("IS_NULLABLE", ((JSONObject)obj1).get("is_NULLABLE").toString());
          map.put("DATA_TYPE", ((JSONObject)obj1).get("type_NAME").toString());
          map.put("EXTRA", ((JSONObject)obj1).get("is_AUTOINCREMENT").toString());
          list1.add(map);
      }
     map1.put(tableName, list1);
    }
   return  map1;
  } 
  
  public List<Map<String, Object>> selectKylinRelevance() {
    login();
    String method = "POST";
    String para = "/query";
    JSONObject jsonParam = new JSONObject();
    jsonParam.put("sql", sql);
    jsonParam.put("limit", "20");
    jsonParam.put("project",kylinProjectName);
    String result = excute(para, method, jsonParam.toString());
    JSONObject json = JSONObject.fromObject(result);
    JSONArray columnMetas = (JSONArray)json.get("columnMetas");
    List<Map<String, Object>> list = new ArrayList<>();
    for(Object obj:columnMetas){
      Map<String, Object> map = new HashMap();
      map.put("COLUMN_NAME", ((JSONObject)obj).get("label").toString());
      map.put("IS_NULLABLE", ((JSONObject)obj).get("isNullable").toString());
      map.put("DATA_TYPE", ((JSONObject)obj).get("columnTypeName").toString());
      map.put("EXTRA", ((JSONObject)obj).get("autoIncrement").toString());
      list.add(map);
    }
   return list ;
  } 
}
