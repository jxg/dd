package com.liangzhi.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import com.liangzhi.entity.JfRole;
import com.liangzhi.service.JfRoleService;
import com.jjmf.util.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
* Created by jiang.
* 时间:2018-05-28 16:47:34
*/
@Api("freeMarker控制器")
@RestController
@RequestMapping("JfRole")
public class JfRoleController {

    @Autowired
    private JfRoleService service;

     @ApiOperation(value = "报表权限添加", notes = "报表权限添加", produces = "application/json")
  @ResponseBody
  @RequestMapping(value = "/addControl", method = {RequestMethod.POST})
  public String addControl(@RequestBody Map<String, String> map) {
    String[] splits = map.get("reportIds").split(",");
    List<JfRole> list = new ArrayList();
    for (String split : splits) {
      JfRole jfRole = new JfRole();
      int reportId = Integer.parseInt(split);
      jfRole.setRoleId(map.get("roleId"));
      jfRole.setMenuId(reportId);
      list.add(jfRole);
    }
    String result = service.updateControl(list);
    return result;
  }
}
