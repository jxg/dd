package com.liangzhi.service.Impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.liangzhi.dao.JfRoleDao;
import com.liangzhi.entity.JfRole;
import com.liangzhi.service.JfRoleService;
import com.jjmf.util.entity.*;
import com.jjmf.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


/**
* Created by jiang.
* 时间:2018-05-28 16:47:34
*/
@Service
@Transactional
public class JfRoleServiceImpl implements JfRoleService {

    @Autowired
    private JfRoleMapper mapper;

   @Override
  public String updateControl(List<JfRole> list) {
    
    JfRole jfRole = new JfRole();
    StringBuilder roleId = new StringBuilder();
    roleId.append("'").append(list.get(0).getRoleId()).append("'");
    jfRole.setRoleId(roleId.toString());
    List<Map<String, String>> selectControl = mapper.selectControl(jfRole);
    if (selectControl.isEmpty()) {
      try {
        mapper.addControl(list);
        return JsonResult.successJson();
      } catch (Exception e) {
        e.printStackTrace();
        return JsonResult.errorJson("add control error");
      }
    } else {
      try {
        JfRole jfRole1 = new JfRole();
        jfRole1.setRoleId(list.get(0).getRoleId());
        mapper.deleteControl(jfRole1);
        mapper.addControl(list);
        return JsonResult.successJson();
      } catch (Exception e) {
        e.printStackTrace();
        return JsonResult.errorJson("update control error");
      }
    }
  }
}
