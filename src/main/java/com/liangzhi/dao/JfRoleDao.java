package com.liangzhi.dao;

import com.liangzhi.entity.JfRole;
import org.apache.ibatis.annotations.*;

import java.util.*;

/**
* Created by jiang.
* 时间:2018-05-28 16:47:34
*/
@Mapper
public interface JfRoleDao {

    String getControl(String reportId);

    List<Map<String, String>> selectControl(JfRole jfRole);

    void addControl(@Param("lists")List<JfRole> list);

    void deleteControl(JfRole jfRole);

}
