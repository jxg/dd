package com.liangzhi.entity;


import org.springframework.format.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
* Created by jiang.
* 时间:2018-05-28 16:47:34
*/
@ApiModel("实体类")
public class JfRole {
     
    private Integer id;     
     
    private String roleId;     
     
    private String roleDesc;     
     
    private String updateTime;     
     
    private String rank;     
    /******************
    * 无参构造器
    *********************/
    public JfRole() {
    }

    /*****************
    *有参构造器
    *********************/
    public JfRole(Integer id,String roleId,String roleDesc,String updateTime,String rank) {
        this.id = id;
        this.roleId = roleId;
        this.roleDesc = roleDesc;
        this.updateTime = updateTime;
        this.rank = rank;
}

    /****************
    *set和get方法
    ****************/
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }
    public String getRoleDesc() {
        return roleDesc;
    }

    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc;
    }
    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    /*****************
    *toString
    ***********************/
    @Override
    public String toString() {
        return "JfRole{" +
            "id='" + id + '\'' +
            "roleId='" + roleId + '\'' +
            "roleDesc='" + roleDesc + '\'' +
            "updateTime='" + updateTime + '\'' +
            "rank='" + rank + '\'' +
        '}';
    }
}
