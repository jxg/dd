/*<!--提交-->*/
var d = {};

function submitForm(a) {
	$(a).form('submit', {
		onSubmit: function() {
			if(!$(this).form('enableValidation').form('validate')) { //表单提交前先校验
				return $(this).form('enableValidation').form('validate');
			};
			//获取表单中所有值
			var t = $(a).serializeArray();
			$.each(t, function() {
				d[this.name] = this.value;
			});
			//alert(JSON.stringify(d)); //JSON转为字符
			//查看当前节点有多少子节点
			var node = $('#project-tree').tree('getSelected');
			var children_length = 0;
			if(node.children) {
				children_length = node.children.length;
			} else {
				children_length = 0;
			};
			//批量添加数据
			if(d.num < children_length) {
				alert('添加时请输入大于当前节点数的数值');
			};
			if(d.num > children_length) {
				for(var i = children_length; i < d.num; i++) {
					append(i);
				}
			};
		}
	});
}
/*重置*/
function clearForm(a) {
	$(a).form('clear');
}
/*页面加载时加载*/
$(function() {
	tree_load;
});
/*树加载数据*/
var tree_load = $('#project-tree').tree({
	url: 'json/poject-details.json',
	method: 'get',
	animate: true,
	formatter: function(node) { //添加统计
		var name = ['项目', '地块', '楼栋', '单元', '户'];
		var s = node.text;
		if(node.children) {
			s += '&nbsp;<span style=\'color:red\'>(' + node.children.length + name[node.attributes.add_son] + ')</span>';
		}
		return s;
	},
	onContextMenu: function(e, node) {
		e.preventDefault();
		$(this).tree('select', node.target);
		$('#mm').menu('show', {
			left: e.pageX,
			top: e.pageY
		});
	},
	onDblClick: function(node) { //双击改名称
		$(this).tree('beginEdit', node.target);
	},
	onAfterEdit: function(node) { //编辑节点后事件
		$('#plot-panel').panel({ title: node.text });
		reload_panel(node);
		//ajax向后台传送修改数据
	},
	onClick: function(node) { //单击改属性
		reload_panel(node);
	}

});

/*<!--树菜单右键添加事件-->*/
function append(num) {
	var t = $('#project-tree');
	var node = t.tree('getSelected');
	var name = ['项目', '地块', '楼栋', '单元', '0'];
	if(!num) {
		num = 1;
	}
	if(node.children) {
		num = node.children.length + 1;
	}
	if(node.attributes.add_son == 5) {
		$('#dlg').html('已经到户，不能够再添加了！');
		$('#dlg').dialog({ title: '&nbsp;温馨提示：' });
		$('#dlg').dialog('open');
		return;
	}
	t.tree('append', {
		parent: (node ? node.target : null),
		data: [{
			"id": UUID(), //UUID前台添加的
			text: name[node.attributes.add_son] + num,
			"attributes": {
				"add_son": node.attributes.add_son + 1
			}
		}]
	});
	reload_panel(node);
}
/*<!--树菜单右键删除事件-->*/
function removeit() {
	var node = $('#project-tree').tree('getSelected');
	if(node.attributes.add_son == 1) { //如果为顶级“项目”不能够删除
		$('#dlg').html('项目不能删除！');
		$('#dlg').dialog({ title: '&nbsp;温馨提示：' });
		$('#dlg').dialog('open');
		return;
	}
	$('#project-tree').tree('remove', node.target);
}
/*<!--树菜单右键修改事件-->*/
function edit() {
	var t = $('#project-tree');
	var node = t.tree('getSelected');
	$('#project-tree').tree('beginEdit', node.target);
	reload_panel(node);
}
/*重新加载详情面板*/
var f = "";
/*加载信息窗口*/
function reload_panel(node) {
	/*调用递归查其父类,叠加标题*/
	f = ""; //递归方法调用前先清空
	getFarther(node);
	var tittle_name = "";
	if(!f) {
		tittle_name = node.text;
	} else {
		tittle_name = f + node.text;
	}
	/*加载时先隐藏所有面板*/
	$('#project-panel').panel('close');
	$('#plot-panel').panel('close');
	$('#generic-panel').panel('close');
	$('#element-panel').panel('close');
	$('#room-num-panel').panel('close');
	var s = '';
	if(node.children) {
		s = node.children.length;
	} else {
		s = 0;
	}
	if(node.attributes.add_son == 1) { //地块信息
		$('#project-panel').panel({ title: tittle_name });
		$('#project-panel').panel('open');
		$('#plot-num').textbox("setValue", s);
		//远程请求信息
	}
	if(node.attributes.add_son == 2) { //地块信息
		$('#plot-panel').panel({ title: tittle_name });
		$('#plot-panel').panel('open');
		$('#generic-num').textbox("setValue", s);
		//远程请求信息
	}
	if(node.attributes.add_son == 3) { //楼栋信息
		$('#generic-panel').panel({ title: tittle_name });
		$('#generic-panel').panel('open');
		$('#element-num').textbox("setValue", s);
	}
	if(node.attributes.add_son == 4) { //单元信息
		$('#element-panel').panel({ title: tittle_name });
		$('#element-panel').panel('open');
		$('#room-num-num').textbox("setValue", s);
		//远程请求信息
	}
	if(node.attributes.add_son == 5) { //户信息
		$('#room-num-panel').panel({ title: tittle_name });
		$('#room-num-panel').panel('open');
		//远程请求信息
	}
}
/*递归查询当前节点的父级标题*/
var getFarther = function(node) {
	var father = $('#project-tree').tree("getParent", node.target);
	if(father) {
		f = father.text + "_" + f;
		getFarther(father);
	} else {
		return f;
	}
}