var JJMF = {
	kingEditorParams: {
		//指定上传文件参数名称
		filePostName: "uploadFile",
		//指定上传文件请求的url。
		uploadJson: '/pic/upload',
		//上传类型，分别为image、flash、media、file
		dir: "image"
	},
	// 格式化时间
	formatDateTime: function(val, row) {
		var now = new Date(val);
		return now.format("yyyy-MM-dd hh:mm:ss");
	},
	// 格式化连接
	formatUrl: function(val, row) {
		if(val) {
			return "<a href='" + val + "' target='_blank'>查看</a>";
		}
		return "";
	},
	// 格式化价格
	formatPriceMode: function(val, row) {
		var mode = "";
		if(val == 'BUILD_AREA') {
			mode = '<span style="color:green;">建筑面积计价</span>';
		} else {
			mode = '<span style="color:#FF8080;">套内面积计价</span>';
		}
		return mode;
	},
	// 格式化价格
	formatPrice: function(val, row) {
		return val + "元";
	},
	// 格式化总金额 
	formatMoney: function(val, row) {
		var s = (val / 10000).toFixed(2);
		return s + "万";
	},
	// 格式化商品的状态
	formatRoomStatus: function formatStatus(val, row) {
		if(val == 1) {
			return '<span style="color:#FF8080;">非售房</span>';
		} else if(val == 2) {
			return '<span style="color:#FF8080;">销控房</span>';
		} else if(val == 3) {
			return '<span style="color:green;">预约房</span>';
		} else if(val == 4) {
			return '<span style="color:#E68900;">预订房</span>';
		} else if(val == 5) {
			return '<span style="color:#E68900;">认购房</span>';
		} else if(val == 6) {
			return '<span style="color:red;">签约房</span>';
		} else if(val == 0) {
			return '<span style="color:green;">在售房</span>';
		} else {
			return '未知';
		}
	},
	//房间历史价格
	formatRoomLSJG: function formatRoomLSJG(val, row) {
		return "&nbsp;&nbsp;<a href='#' onclick='lsjg(" + row.ROOM_ID + ")'>历史价格查询</a>&nbsp;&nbsp;";
	},
	//房间历史操作
	formatRoomCZ: function formatRoomCZ(val, row) {
		return "&nbsp;&nbsp;<a href='#' onclick='jgtz(" + row.ROOM_ID + ")'>当前价格调整</a>&nbsp;&nbsp;&nbsp;&nbsp;";
	},
	//房间面积加平方米
	formatRoomArea: function formatRoomArea(val, row) {
		return val+"平方米";
	}

}
