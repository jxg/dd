package com.liangzhi;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.tomcat.util.codec.binary.Base64;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.liangzhi.autoCode.AutoCode;
import com.liangzhi.autoCode.CreateCodeService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/7/7 0007.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class StartFreemarker {

  @Autowired
  private CreateCodeService c;
  /*
   * private static String encoding; public static String sql =
   * "SELECT * FROM AGG_CUST_STATISTICS a LEFT JOIN DIM_ACCOUNT b ON a.ACCOUNT_NO=b.ACCOUNT_NO";
   */



  /*
   * 第一个参数为数据库名，第二个参数为表名，第三个参数为说明及连接那个数据库，（mysql，kylin，hive）中的一种， 如果第三个参数不填则为kylin的单表查询。
   * 如：（"数据库名","表名","mysql"）
   */
  @Test
  // @Ignore
  public void createCode() {
    AutoCode code = new AutoCode("jfpt", "jf_menu", "mysql");
    c.start(code);
  }



  /*
   * @Test // @Ignore public void test() throws ClassNotFoundException, Exception { String
   * driverName = "org.apache.hive.jdbc.HiveDriver"; String url =
   * "jdbc:hive2://192.168.2.59:10000/ctdata"; String user = "hive"; String password = "hive";
   * String sql = "SELECT * FROM dim_branch"; Class.forName(driverName); Connection
   * conn=DriverManager.getConnection(url, user, password);
   * 
   * try { PreparedStatement pstmt=conn.prepareStatement(sql); ResultSet rs=pstmt.executeQuery();
   * ResultSetMetaData md = rs.getMetaData(); //获得结果集结构信息,元数据 int columnCount = md.getColumnCount();
   * //获得列数
   * 
   * List<Map<String, Object>> list = new ArrayList<Map<String,Object>>(); Map<String,Object>
   * rowData = new HashMap<String,Object>(); for (int i = 1; i <= columnCount; i++) {
   * rowData.put(md.getColumnName(i), md.getColumnType(i)); System.out.println(md.getColumnName(i)+
   * md.getColumnTypeName(i)); } list.add(rowData); } catch (Exception e) { e.printStackTrace(); }
   * finally{ conn.close(); } }
   * 
   * 
   * 
   * public static String login() { String method = "POST"; String para = "/user/authentication";
   * byte[] key = ("ADMIN" + ":" + "KYLIN").getBytes(); encoding = Base64.encodeBase64String(key);
   * return excute(para, method, null); }
   * 
   * private static String excute(String para, String method, String body) {
   * 
   * StringBuilder out = new StringBuilder(); try { URL url = new
   * URL("http://192.168.2.59:8888/notebook/api/" + para); HttpURLConnection connection =
   * (HttpURLConnection) url.openConnection(); connection.setRequestMethod(method);
   * connection.setDoOutput(true); connection.setRequestProperty("Authorization", "Basic " +
   * encoding); connection.setRequestProperty("Content-Type", "application/json"); if (body != null)
   * { byte[] outputInBytes = body.getBytes("UTF-8"); OutputStream os =
   * connection.getOutputStream(); os.write(outputInBytes); os.close(); } InputStream content =
   * (InputStream) connection.getInputStream(); BufferedReader in = new BufferedReader(new
   * InputStreamReader(content)); String line; while ((line = in.readLine()) != null) {
   * out.append(line); } in.close(); connection.disconnect();
   * 
   * } catch (Exception e) { e.printStackTrace(); } System.out.println(out.toString()); return
   * out.toString(); }
   */


}
